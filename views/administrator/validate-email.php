<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $model app\models\Administrator */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="password-form col-md-offset-4 col-md-3">
    <?php $form = ActiveForm::begin([
        'id' => 'validate-email',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Reset', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
