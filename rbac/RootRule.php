<?php

namespace app\rbac;
/**
 * Created by PhpStorm.
 * User: hd25
 * Date: 20.06.16
 * Time: 10:17
 */
use yii;
use yii\rbac\Rule;

class RootRule extends Rule
{
    public $name = 'isRoot';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->isRoot()) {
            return true;
        }

        return false;
    }
}