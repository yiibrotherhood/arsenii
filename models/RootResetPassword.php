<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 21.06.16
 * Time: 12:32
 */

namespace app\models;

use yii;
use yii\base\Model;

class RootResetPassword extends Model
{
    public $newPassword;
    public $repeatNewPassword;

    public function rules()
    {
        return [
            [['newPassword', 'repeatNewPassword'], 'required'],
            [['newPassword', 'repeatNewPassword'], 'string', 'min' => 5],
            ['repeatNewPassword', 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'newPassword' => 'New password',
            'repeatNewPassword' => 'Repeat new password',
        ];
    }

    public function sendEmail($user)
    {
        if (!$user)
            return false;

        return Yii::$app->mailer->compose('@app/mail/ResetPassword', ['user' => $user])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($user->email)
            ->setSubject('Confirm reset password')
            ->send();
    }

    public function save($slug)
    {
        $user = Administrator::findOne(['slug' => $slug]);
        if (!$user)
            return false;

        if ($this->validate()) {
            $user->setPassword($this->newPassword);
            if ($user->save(false)) {
                $this->sendEmail($user);
                return true;
            }

        }
        return false;
    }

}