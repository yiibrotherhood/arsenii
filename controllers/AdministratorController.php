<?php

namespace app\controllers;

use app\models\Companies;
use app\models\CompaniesSearch;
use yii;
use app\models\ActivateForm;
use app\models\LoginForm;
use app\models\PasswordForm;
use app\models\ResetPasswordForm;
use app\models\RootResetPassword;
use app\models\SignUpForm;
use app\models\ValidateEmail;
use app\models\Administrator;
use app\models\AdministratorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

/**
 * AdministratorController implements the CRUD actions for Administrator model.
 */
class AdministratorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'sign-up', 'password', 'update', 'delete', 'view', 'index', 'reset-password', 'root-reset-password', 'success', 'validate-email'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'sign-up', 'reset-password', 'success', 'validate-email'],
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'password', 'update', 'view', 'index',],
                        'allow' => true,
                        'roles' => ['admin', 'root'],
                    ],
                    [
                        'actions' => ['delete', 'root-reset-password',],
                        'allow' => true,
                        'roles' => ['root'],
                    ]

                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionActivate($token)
    {
        $model = new ActivateForm();

        if ($model->activate($token)) {
            return $this->render('success', ['success' => 'Activation is done.']);
        }

        return $this->render('error', ['error' => 'Activation is false.']);
    }

    public function actionValidateEmail()
    {
        $model = new ValidateEmail();
        if ($model->load(Yii::$app->request->post()) && $model->validateEmail()) {
            if ($model->sendEmail()) {
                return $this->render('success', ['success' => 'Check your email']);
            } else
                return $this->render('error', ['error' => 'Some problems']);
        }
        return $this->render('validate-email', ['model' => $model]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->render('success', ['success' => 'Your password is update']);
        }

        return $this->render('reset-password', ['model' => $model]);
    }

    public function actionRootResetPassword($slug)
    {
        $model = new RootResetPassword();

        if (!Yii::$app->user->can('canResetPassword', ['slug' => $slug]))
            throw new ForbiddenHttpException('Access denied.');

        if ($model->load(Yii::$app->request->post()) && $model->save($slug)) {
            return $this->redirect(['view', 'slug' => $slug]);
        }

        return $this->render('root-reset-password', ['model' => $model]);
    }

    public function actionUpdate($slug)
    {
        $model = $this->findModelBySlug($slug);
        $companies = CompaniesSearch::getArrayCompanies();
        $auth = Yii::$app->authManager;

        if (!Yii::$app->user->can('updateOwnProfile', ['slug' => $slug]))
            throw new ForbiddenHttpException('Access denied');

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->user->identity->isRoot()) {
                if ($model->group_role == 'root') {
                    $auth->revokeAll($model->id);
                    $auth->assign($auth->getRole('root'), $model->id);
                }

                if ($model->group_role == 'admin') {
                    $auth->revokeAll($model->id);
                    $auth->assign($auth->getRole('admin'), $model->id);
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'slug' => $model->slug]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'companies' => $companies,
        ]);


    }

    public function actionIndex()
    {
        $searchModel = new AdministratorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionView($slug)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('view', [
                'model' => $this->findModelBySlug($slug),
            ]);
        } else {
            $model = new SignUpForm();
            return $this->render('sign-up', ['model' => $model]);
        }
    }

    /**
     * Creates a new Administrator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSignUp()
    {
        $model = new SignUpForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $user->setRoleAdmin($user->id);
                if ($model->sendEmail($user->email)) {
                    return $this->render('success', ['success' => 'Activation message has been send. Please check your email.']);
                }
            }
        }
        return $this->render('sign-up', [
            'model' => $model,
        ]);
    }

    public function actionPassword($slug)
    {
        $model = new PasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'slug' => $slug]);
        } else {
            return $this->render('password', ['model' => $model]);
        }
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionDelete($slug)
    {
        $model = $this->findModelBySlug($slug);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Administrator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Administrator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Administrator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModelBySlug($slug)
    {
        if (($model = Administrator::findOne(['slug' => $slug])) !== null)
            return $model;
        else
            throw new NotFoundHttpException("The requested page does not exist.");
    }
}
