<?php

use yii\db\Schema;
use yii\db\Migration;

class m170219_153928_user extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> $this->primaryKey(11),
                'email'=> $this->string(255)->notNull(),
                'status'=> $this->smallInteger(6)->notNull()->defaultValue(10),
                'updated_at'=> $this->integer(11)->null()->defaultValue(null),
                'password_hash'=> $this->string(255)->notNull(),
                'password_reset_token'=> $this->string(255)->null()->defaultValue(null),
                'auth_key'=> $this->string(32)->notNull(),
                'created_at'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('email','{{%user}}','email',true);
        $this->createIndex('password_reset_token','{{%user}}','password_reset_token',true);
    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%user}}');
        $this->dropIndex('password_reset_token', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
