<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
/* @var $model app\models\Administrator */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Password: ' . Yii::$app->user->identity->name;
$this->params['breadcrumbs'][] = ['label' => 'Administrators', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->identity->name, 'url' => ['view', 'slug' => Yii::$app->user->identity->slug]];
$this->params['breadcrumbs'][] = 'Change password';
?>

<div class="password-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'oldPass')->passwordInput() ?>
    <?= $form->field($model, 'newPass')->passwordInput() ?>
    <?= $form->field($model, 'repeatPass')->passwordInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Change', ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
