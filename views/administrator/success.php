<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 15:50
 */
/* @var $success result of activate */
?>
<div class="alert alert-success">
    <strong><?= $success ?></strong>
</div>