<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 17:13
 */

namespace app\models;

use yii;
use yii\base\Model;
use yii\base\InvalidParamException;

class ResetPasswordForm extends Model
{
    public $newPass;
    public $repeatPass;

    private $_user = false;

    public function rules()
    {
        return [
            [['newPass', 'repeatPass'], 'required'],
            [['newPass', 'repeatPass'], 'string', 'min' => 5],
            ['repeatPass', 'compare', 'compareAttribute' => 'newPass'],
        ];
    }

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = Administrator::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->newPass);
        $user->setPasswordResetToken();
        return $user->save(false);
    }

    public function attributeLabels()
    {
        return [
            'newPass' => 'New password',
            'repeatPass' => 'Repeat new password',
        ];
    }
}