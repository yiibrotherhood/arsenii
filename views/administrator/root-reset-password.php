<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 21.06.16
 * Time: 12:30
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please choose your new password:</p>

    <div class="row">
        <div class="col-md-offset-4 col-md-3">
            <?php $form = ActiveForm::begin([
                'id' => 'root-reset-password',
                'options' => ['class' => 'form-horizontal'],
            ]); ?>

            <?= $form->field($model, 'newPassword')->passwordInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'repeatNewPassword')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Reset', ['class' => 'btn btn-warning']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>