<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "administrator".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $slug
 * @property integer $company_id
 * @property string $email
 * @property string $auth_key
 * @property string $password_reset_token_hash
 * @property string $password_hash
 * @property string $group_role
 * @property string $status
 */
class Administrator extends ActiveRecord implements IdentityInterface
{
    const STATUS_BLOCKED = -1;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'password_hash'], 'required'],
            [['company_id', 'status'], 'integer'],
            [['name', 'surname', 'slug', 'email', 'auth_key', 'password_reset_token_hash', 'password_hash', 'group_role'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_role' => 'Role',
            'status' => 'Status active',
            'name' => 'Name',
            'surname' => 'Surname',
            'slug' => 'Slug',
            'company_id' => 'Company ID',
            'email' => 'Email',
            'password_hash' => 'Password',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name', 'surname'],
                'ensureUnique' => true,
                //'slugAttribute' => 'slug',
            ],
        ];
    }

    public function setCompanyId($id)
    {
        $this->company_id = $id;
    }

    public function setCompanyIdRandom()
    {
        $companiesCount = Companies::find()->count();
        if ($companiesCount == 0)
            return null;
        $randomId = rand(1, $companiesCount);
        $this->company_id = $randomId;
    }

    public function setPassword($password)
    {
        $this->password_hash = self::encodePassword($password);
    }

    public function deleteCompany()
    {
        $this->company_id = null;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
                $this->setPasswordResetToken();
                $this->status = self::STATUS_INACTIVE;
            }
            
            return true;
        } else {
            return false;
        }
    }

    public function setRoleAdmin($id)
    {
        $role = Yii::$app->authManager->getRole('admin');
        Yii::$app->authManager->assign($role, $id);
    }

    public function setRoleRoot($id)
    {
        $role = Yii::$app->authManager->getRole('root');
        Yii::$app->authManager->assign($role, $id);
    }

    public function revokeRole($role, $id)
    {
        return Yii::$app->authManager->revoke($role, $id);
    }


    public function setPasswordResetToken()
    {
        $this->password_reset_token_hash = Yii::$app->getSecurity()->generateRandomString();
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public static function findByPasswordResetToken($passwordReset)
    {
        return static::findOne(['password_reset_token_hash' => $passwordReset]);
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    public static function encodePassword($password)
    {
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Finds administrator by email
     *
     * @param string $email
     * @return static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds administrator by id
     *
     * @param string $id
     * @return static
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds administrator by id
     *
     * @return true||false
     */
    public function isRoot()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);
        $root = Yii::$app->authManager->getRole('root');
        foreach ($roles as $role)
            if ($role == $root)
                return true;
        return false;
    }
}
