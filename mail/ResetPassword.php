<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator*/
$viewLink = Yii::$app->urlManager->createAbsoluteUrl(['administrator/view', 'slug' => $user->slug]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Your password had been reset:</p>

    <p><?= Html::a(Html::encode($viewLink), $viewLink) ?></p>
</div>