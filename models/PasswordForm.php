<?php
/**
 * Created by PhpStorm.
 * @author: hd25
 * Date: 15.06.16
 * Time: 08:36
 */

namespace app\models;

use yii;
use yii\base\Model;

class PasswordForm extends Model
{
    public $oldPass;
    public $newPass;
    public $repeatPass;

    private $_user = false;

    public function rules()
    {
        return [
            [['oldPass', 'newPass', 'repeatPass'], 'required'],
            [['oldPass', 'newPass', 'repeatPass'], 'string', 'min' => 5],
            ['oldPass', 'validatePassword'],
            ['repeatPass', 'compare', 'compareAttribute' => 'newPass'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldPass)) {
                $this->addError($attribute, 'Old password mismatch');
            }
        }
    }

    public function save()
    {
        $user = $this->getUser();
        if ($this->validate()) {
            $user->password_hash = Administrator::encodePassword($this->newPass);
            $user->save();

            return true;
        }
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Administrator::findByEmail(Yii::$app->user->identity->email);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'oldPass' => 'Old password',
            'newPass' => 'New password',
            'repeatPass' => 'Repeat new password',
        ];
    }
}