<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 15:50
 */
/* @var $error result of activate */
?>

<div class="alert alert-danger">
    <strong><?= $error ?></strong>
</div>