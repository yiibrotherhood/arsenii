<?php
namespace app\commands;

use yii;
use yii\console\Controller;
use app\rbac\UserGroupRule;
use app\rbac\UserProfileOwnerRule;
use app\rbac\RootRule;
use app\rbac\RootResetPasswordRule;
use app\rbac\UserStatusRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;
        $authManager->removeAll();

        $admin = $authManager->createRole('admin');
        $root = $authManager->createRole('root');

        $login = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');
        $index = $authManager->createPermission('index');
        $view = $authManager->createPermission('view');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');
        $password = $authManager->createPermission('password');
        $success = $authManager->createPermission('success');
        $resetPassword = $authManager->createPermission('reset-password');
        $validateEmail = $authManager->createPermission('validate-email');
        $rootResetPassword = $authManager->createPermission('root-reset-password');

        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($update);
        $authManager->add($delete);
        $authManager->add($password);
        $authManager->add($success);
        $authManager->add($resetPassword);
        $authManager->add($validateEmail);
        $authManager->add($rootResetPassword);


        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);
        $root->ruleName = $userGroupRule->name;
        $admin->ruleName = $userGroupRule->name;

        $userProfileOwnerRule = new UserProfileOwnerRule();
        $authManager->add($userProfileOwnerRule);
        $updateOwnProfile = $authManager->createPermission('updateOwnProfile');
        $updateOwnProfile->ruleName = $userProfileOwnerRule->name;
        $authManager->add($updateOwnProfile);

        $rootRule = new RootRule();
        $authManager->add($rootRule);
        $isRoot = $authManager->createPermission('isRoot');
        $isRoot->ruleName = $rootRule->name;
        $authManager->add($isRoot);
        
        $rootResetPasswordRule = new RootResetPasswordRule();
        $authManager->add($rootResetPasswordRule);
        $canResetPassword = $authManager->createPermission('canResetPassword');
        $canResetPassword->ruleName = $rootResetPasswordRule->name;
        $authManager->add($canResetPassword);

        $userStatusRule = new UserStatusRule();
        $authManager->add($userStatusRule);
        $isActive = $authManager->createPermission('isActive');
        $isActive->ruleName = $userStatusRule->name;
        $authManager->add($isActive);

        $authManager->add($admin);
        $authManager->add($root);

        $authManager->addChild($admin, $login);
        $authManager->addChild($admin, $error);
        $authManager->addChild($admin, $success);
        $authManager->addChild($admin, $signUp);
        $authManager->addChild($admin, $index);
        $authManager->addChild($admin, $isActive);
        $authManager->addChild($admin, $password);
        $authManager->addChild($admin, $updateOwnProfile);
        $authManager->addChild($admin, $logout);
        $authManager->addChild($admin, $view);
        $authManager->addChild($admin, $update);

        $authManager->addChild($root, $delete);
        $authManager->addChild($root, $isRoot);
        $authManager->addChild($root, $rootResetPassword);
        $authManager->addChild($root, $canResetPassword);
        $authManager->addChild($root, $admin);

    }
}