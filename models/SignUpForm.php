<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii Andrieiev
 * Date: 15.06.16
 * Time: 13:55
 */

namespace app\models;

use yii;
use yii\base\Model;
use yii\helpers\Html;

/**
 * This is the model class for table "administrator".
 *
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password
 */
class SignUpForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'password'], 'required'],
            [['name', 'surname', 'email'], 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\Administrator', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 5],
            [['name', 'surname', 'email'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    public function sendEmail($email)
    {
        //@todo change to administrator->email change
        $administrator = Administrator::findByEmail($email);

        return Yii::$app->mailer->compose('@app/mail/ActivateProfile', ['user' => $administrator])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($administrator->email)
            ->setSubject('Activate your account')
            ->send();
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $administrator = new Administrator();
        $administrator->name = $this->name;
        $administrator->surname = $this->surname;
        $administrator->email = $this->email;
        $administrator->setPassword($this->password);
        $administrator->setCompanyIdRandom();
        $administrator->group_role = Yii::$app->authManager->getRole('admin')->name;

        return $administrator->save() ? $administrator : null;
        
    }

}