<?php
return [
    'login' => [
        'type' => 2,
    ],
    'logout' => [
        'type' => 2,
    ],
    'error' => [
        'type' => 2,
    ],
    'sign-up' => [
        'type' => 2,
    ],
    'index' => [
        'type' => 2,
    ],
    'view' => [
        'type' => 2,
    ],
    'update' => [
        'type' => 2,
    ],
    'delete' => [
        'type' => 2,
    ],
    'password' => [
        'type' => 2,
    ],
    'success' => [
        'type' => 2,
    ],
    'reset-password' => [
        'type' => 2,
    ],
    'validate-email' => [
        'type' => 2,
    ],
    'root-reset-password' => [
        'type' => 2,
    ],
    'updateOwnProfile' => [
        'type' => 2,
        'ruleName' => 'isProfileOwner',
    ],
    'isRoot' => [
        'type' => 2,
        'ruleName' => 'isRoot',
    ],
    'canResetPassword' => [
        'type' => 2,
        'ruleName' => 'rootResetPassword',
    ],
    'isActive' => [
        'type' => 2,
        'ruleName' => 'isActive',
    ],
    'guest' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'login',
            'error',
            'success',
            'sign-up',
            'index',
            'isActive',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'password',
            'updateOwnProfile',
            'logout',
            'view',
            'update',
            'guest',
        ],
    ],
    'root' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'delete',
            'isRoot',
            'root-reset-password',
            'canResetPassword',
            'admin',
        ],
    ],
];
