<?php
/**
 * Created by PhpStorm.
 * User: Arsenii
 * Date: 20.06.16
 * Time: 12:06
 */
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator*/
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['administrator/reset-password', 'token' => $user->PASSWORD_RESET_TOKEN_HASH]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>