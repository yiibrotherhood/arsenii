<?php

namespace app\rbac;
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 21.06.16
 * Time: 13:49
 */

use yii;
use yii\rbac\Rule;
use app\models\Administrator;

class UserStatusRule extends Rule
{
    public $name = 'isActive';

    public function execute($user, $item, $params)
    {
        $user = Administrator::findByEmail($params['email']);
        
        if (!$user)
            return false;
        
        if ($user->status == Administrator::STATUS_ACTIVE) {
            return true;
        }

        return false;
    }
}