<?php

use yii\db\Schema;
use yii\db\Migration;

class m170219_153926_companies extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%companies}}',
            [
                'id'=> $this->primaryKey(11),
                'name'=> $this->string(255)->null()->defaultValue(null),
                'description'=> $this->string(255)->null()->defaultValue(null),
            ],$tableOptions
        );
        $this->createIndex('name','{{%companies}}','name',true);
    }

    public function safeDown()
    {
        $this->dropIndex('name', '{{%companies}}');
        $this->dropTable('{{%companies}}');
    }
}
