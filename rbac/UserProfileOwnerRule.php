<?php
namespace app\rbac;

use yii;
use yii\rbac\Rule;

class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->isRoot()) {
            return true;
        }

        return isset($params['slug']) ? Yii::$app->user->identity->slug == $params['slug'] : false;
    }
}