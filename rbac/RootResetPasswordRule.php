<?php

namespace app\rbac;
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 21.06.16
 * Time: 12:08
 */

use yii;
use yii\rbac\Rule;
use yii\rbac\Item;

class RootResetPasswordRule extends Rule
{
    public $name = 'rootResetPassword';

    /**
     * @param string|integer $user   the user slug.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->isRoot() && isset($params['slug'])) {
            if (Yii::$app->user->identity->slug != $params['slug'])
                return true;                
        }

        return false;
    }
}