<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\PasswordForm;
use app\models\SignUpForm;
use yii;
use app\models\Administrator;
use app\models\AdministratorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotAcceptableHttpException;
use yii\web\ForbiddenHttpException;

/**
 * AdministratorController implements the CRUD actions for Administrator model.
 */
class AdministratorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'sign-up', 'password', 'update', 'delete', 'view'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'sign-up'],
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'password', 'update', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function ($rule, $action) {
                            throw new ForbiddenHttpException();
                        },
                        'matchCallback' => function ($rule, $action) {
                            $role = Yii::$app->user->identity->group_role;
                            if ($role == Administrator::ROLE_ROOT)
                                return true;
                            else
                                return false;
                        },
                    ]

                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /*    public function beforeAction($action)
        {
            if (parent::beforeAction($action)) {
                if (!\Yii::$app->user->can($action->id)) {
                    throw new ForbiddenHttpException('Access denied');
                }
                return true;
            } else {
                return false;
            }
        }*/

    public function actionIndex()
    {
        $searchModel = new AdministratorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionView($slug)
    {

        if (!Yii::$app->user->isGuest) {
            return $this->render('view', [
                'model' => $this->findModelBySlug($slug),
            ]);
        } else {
            $model = new SignUpForm();
            return $this->render('sign-up', ['model' => $model]);
        }
    }

    /**
     * Creates a new Administrator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSignUp()
    {
        $model = new SignUpForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        return $this->render('sign-up', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($slug)
    {
        $model = $this->findModelBySlug($slug);
        $currUserSlug = Yii::$app->user->identity->slug;
        $role = Yii::$app->user->identity->group_role;
        if ((Yii::$app->user->identity->slug == $model->slug) || ($role == Administrator::ROLE_ROOT)) {
            if ($model->load(Yii::$app->request->post())) {
                if ($role != Administrator::ROLE_ROOT) {
                    $model->group_role = Administrator::ROLE_ADMIN;
                    if ($model->save())
                        return $this->redirect(['view', 'slug' => $model->slug]);
                } else {
                    if ($model->save())
                        return $this->redirect(['view', 'slug' => $model->slug]);
                }

            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new NotAcceptableHttpException("You can't modify profile other users");
        }
    }


    public function actionPassword($slug)
    {
        $model = new PasswordForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'slug' => $slug]);
        } else {
            return $this->render('password', ['model' => $model]);
        }
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->status == Administrator::BLOCKED) {
                Yii::$app->user->logout();
                return $this->goHome();
            }

            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionDelete($slug)
    {
        $model = $this->findModelBySlug($slug);
        $currUserSlug = Yii::$app->user->identity->slug;
        $role = Yii::$app->user->identity->group_role;
        if (($currUserSlug == $model->slug) || ($role == Administrator::ROLE_ROOT)) {
            $model->delete();
        } else {
            throw new NotAcceptableHttpException("You can't delete other users");
        }

        return $this->redirect(['index']);

    }

    /**
     * Finds the Administrator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Administrator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Administrator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModelBySlug($slug)
    {
        if (($model = Administrator::findOne(['slug' => $slug])) !== null)
            return $model;
        else
            throw new NotFoundHttpException("The requested page does not exist.");
    }
}
