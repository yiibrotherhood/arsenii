<?php
namespace app\rbac;

use yii;
use yii\rbac\Rule;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $group = Yii::$app->user->identity->group_role;
            if ($item->name === 'root') {
                return $group == 'root';
            } elseif ($item->name === 'admin') {
                return $group == 'root' || $group == 'admin';
            }
        }
        return false;
    }
}