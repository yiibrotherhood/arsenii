<?php

namespace app\models;

use app\controllers\AdministratorController;
use yii;

/**
 * This is the model class for table "companies".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }
    
    
    public function beforeSave($insert)
    {
        $this->name = Yii::$app->user->identity->name;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $administrator = Administrator::findById(Yii::$app->user->identity->id);
        $administrator->setCompanyId($this->id);
        $administrator->save();
    }

    public function beforeDelete()
    {
        parent::beforeDelete();
        $administrator = Administrator::findById(Yii::$app->user->identity->id);
        $administrator->deleteCompany();
        $administrator->save();
    }

    public static function findCompanyById($id)
    {
        return static::findOne(['id' => $id]);
    }
}
