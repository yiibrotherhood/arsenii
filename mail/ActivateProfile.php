<?php
/**
 * Created by PhpStorm.
 * User: Arsenii
 * Date: 20.06.16
 * Time: 12:06
 */
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator*/
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['administrator/activate', 'token' => $user->auth_key]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Activate your account:</p>

    <a class="btn btn-success"><?= Html::a(Html::encode($resetLink), $resetLink) ?></a>
</div>