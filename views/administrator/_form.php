<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */
/* @var $companies app\models\Companies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="administrator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>



    <?php if (Yii::$app->user->identity->isRoot()) {
        echo $form->field($model, 'company_id')->dropDownList($companies);
        echo $form->field($model, 'group_role')->dropDownList(['root' => 'root', 'admin' => 'admin']);
        echo $form->field($model, 'status')->dropDownList(['-1' => 'Blocked', '0' => 'In active', '1' => 'Active']);
    } ?>


    <?php if ($model->isNewRecord) { ?>

        <?= $form->field($model, 'password_hash')->passwordInput(['value' => '']) ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Sign up' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="form-group">
            <?= Html::a('Change password', ['password', 'slug' => $model->slug], ['class' => 'btn btn-default']) ?>

            <?php if (Yii::$app->user->identity->isRoot())  echo Html::a('Reset password', ['root-reset-password', 'slug' => $model->slug], ['class' => 'btn btn-warning']) ?>
        </div>
    <?php } ?>

</div>
