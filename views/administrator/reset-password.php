<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 17:15
 */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\ResetPasswordForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-reset-password">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please choose your new password:</p>

    <div class="row">
        <div class="col-md-offset-4 col-md-3">
            <?php $form = ActiveForm::begin(['id' => 'reset-password']); ?>

            <?= $form->field($model, 'newPass')->passwordInput(['autofocus' => true]) ?>
            
            <?= $form->field($model, 'repeatPass')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>