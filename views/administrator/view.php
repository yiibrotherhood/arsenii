<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Administrators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="administrator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ((Yii::$app->user->identity->slug == $model->slug) || (Yii::$app->user->identity->isRoot())) { ?>
            <?= Html::a('Update', ['update', 'slug' => $model->slug], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'slug' => $model->slug], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?php if (Yii::$app->user->identity->company_id == null) {
                echo Html::a('Create company', ['companies/create'], ['class' => 'btn btn-default']);
            } else {
                echo Html::a('View company', ['companies/view', 'id' => $model->company_id], ['class' => 'btn btn-default']);
            } ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'slug',
            'company_id',
            'email:email',
            'group_role',
            'status',
        ],
    ]) ?>


</div>
