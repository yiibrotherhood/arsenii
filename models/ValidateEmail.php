<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 17:13
 */

namespace app\models;

use yii;
use yii\base\Model;

class ValidateEmail extends Model
{
    public $email;

    private $_user = false;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\Administrator',
                'filter' => ['status' => Administrator::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    public function sendEmail()
    {
        $user = $this->getUserByEmail();
        if (!$user)
            return false;

        return Yii::$app->mailer->compose('@app/mail/PasswordResetToken', ['user' => $user])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($user->email)
            ->setSubject('Confirm reset password')
            ->send();
    }

    public function validateEmail()
    {
        if ($this->validate())
            if ($this->getUserByEmail() != false)
                if ($this->getUserByEmail()->group_role != 'root')
                    return true;

        return false;
    }

    public function getUserByEmail()
    {
        if ($this->_user === false) {
            $this->_user = Administrator::findByEmail($this->email);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}