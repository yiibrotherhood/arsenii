<?php

use yii\db\Schema;
use yii\db\Migration;

class m170219_153921_administrator extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%administrator}}',
            [
                'id'=> $this->primaryKey(11),
                'name'=> $this->string(255)->null()->defaultValue(null),
                'surname'=> $this->string(255)->null()->defaultValue(null),
                'slug'=> $this->string(255)->null()->defaultValue(null),
                'company_id'=> $this->integer(6)->null()->defaultValue(null),
                'group_role'=> $this->string(255)->null()->defaultValue(null),
                'email'=> $this->string(255)->notNull(),
                'status'=> $this->string(6)->notNull(),
                'password_hash'=> $this->string(255)->notNull(),
                'password_reset_token_hash'=> $this->string(255)->null()->defaultValue(null),
                'auth_key'=> $this->string(32)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('email','{{%administrator}}','email',true);
        $this->createIndex('password_reset_token_hash','{{%administrator}}','password_reset_token_hash',true);
    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%administrator}}');
        $this->dropIndex('password_reset_token_hash', '{{%administrator}}');
        $this->dropTable('{{%administrator}}');
    }
}
