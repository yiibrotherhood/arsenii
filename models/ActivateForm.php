<?php
/**
 * Created by PhpStorm.
 * @author: Arsenii
 * Date: 20.06.16
 * Time: 12:32
 */

namespace app\models;

use yii;
use yii\base\Model;

class ActivateForm extends Model
{

    private $_admin = false;


    public function activate($token)
    {
        if ($this->_admin === false) {
            $_admin = Administrator::findIdentityByAccessToken($token);
        }
        if ($_admin) {
            $_admin->status = Administrator::STATUS_ACTIVE;
            if ($_admin->save(false)) {
                Yii::$app->user->login($_admin);
                return true;
            }
        }
        return false;
    }
    
    
}