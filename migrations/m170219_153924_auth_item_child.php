<?php

use yii\db\Schema;
use yii\db\Migration;

class m170219_153924_auth_item_child extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%auth_item_child}}',
            [
                'parent'=> $this->string(64)->notNull(),
                'child'=> $this->string(64)->notNull(),
            ],$tableOptions
        );
        $this->createIndex('child','{{%auth_item_child}}','child',false);
    }

    public function safeDown()
    {
        $this->dropIndex('child', '{{%auth_item_child}}');
        $this->dropTable('{{%auth_item_child}}');
    }
}
